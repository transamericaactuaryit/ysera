﻿using System;
using System.Collections.Generic;
using System.Web.Http;
using System.Web.Http.Description;
using NLog;
using Ysera.DataModel;
using Ysera.DataModel.Models;

namespace Ysera.Api.Controllers
{
    public class ExecutionController : ApiController
    {
        private readonly ILogger logger = LogManager.GetCurrentClassLogger();
                
        [HttpGet]
        [ResponseType(typeof(List<ExecutionRequestDto>))]
        public IHttpActionResult GetExecutionRequests()
        {
            logger.Debug("In GetExecutionRequests");
            var output = new List<ExecutionRequestDto>();
            List<ExecutionRequest> temp;

            try
            {
                temp = YseraDataManager.GetExecutionRequests();                                                              
                temp.ForEach(x => output.Add(new ExecutionRequestDto(x)));
                return Json(output);
            }
            catch (Exception ex)
            {
                logger.Debug(ex.ToString());
                throw;
            }            
        }

        [HttpGet]
        [ResponseType(typeof(ExecutionRequestDto))]
        public IHttpActionResult GetExecutionRequestDetails(int id)
        {
            logger.Debug($"In GetExecutionRequest({id})");
            ExecutionRequestDto output = null;
            ExecutionRequest temp;

            try
            {
                temp = YseraDataManager.GetExecutionRequest(id);
                output = new ExecutionRequestDto(temp);
                return Json(output);
            }
            catch (Exception ex)
            {
                logger.Debug(ex.ToString());
                throw;
            }
        }


        [ResponseType(typeof(ExecutionStatusDto))]
        [HttpGet]
        public IHttpActionResult GetStatus(int id)
        {
            ExecutionStatusDto output = null;
            ExecutionStatus temp;

            try
            {
                temp = YseraDataManager.GetExecutionStatus(id);
                output = new ExecutionStatusDto(temp);
                return Json(output);
            }
            catch (Exception ex)
            {
                logger.Debug(ex.ToString());
                return InternalServerError(ex);
            }
        }

        [HttpGet]        
        [ResponseType(typeof(List<ExecutionStatusDto>))]
        public IHttpActionResult GetAllStatuses()
        {
            var output = new List<ExecutionStatusDto>();
            List<ExecutionStatus> temp;

            try
            {
                temp = YseraDataManager.GetExecutionStatuses();
                temp.ForEach(x => output.Add(new ExecutionStatusDto(x)));
                return Json(output);
            }
            catch (Exception ex)
            {
                logger.Debug(ex.ToString());
                return InternalServerError(ex);
            }
        }

        public IHttpActionResult InsertExecutionRequest([FromBody]ExecutionRequestDto value)
        {
            try
            {
                var tmpEr = new ExecutionRequest
                {
                    SparkAppId = value.SparkAppId,
                    Description = value.Description,
                    StartTime = DateTime.Now,
                    EndTime = null,
                    ExecutionStatusId = value.ExecutionStatusId
                };

                if (YseraDataManager.InsertExecutionRequest(value))
                    return Ok(true);
                else
                    return InternalServerError();                
            }
            catch (Exception ex)
            {
                logger.Debug(ex.ToString());
                return InternalServerError(ex);
            }
        }        
    }
}