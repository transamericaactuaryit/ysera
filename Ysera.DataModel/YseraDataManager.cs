﻿using NLog;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Ysera.DataModel.Models;

namespace Ysera.DataModel
{
    public static class YseraDataManager
    {
        private static readonly ILogger logger = LogManager.GetCurrentClassLogger();

        static YseraDataManager() => logger.Debug("YseraDataManager started...");

        public static bool InsertExecutionRequest(ExecutionRequestDto er)
        {
            var output = false;

            try
            {
                var tmpEr = new ExecutionRequest
                {
                    SparkAppId = er.SparkAppId,
                    Description = er.Description,
                    StartTime = DateTime.Now,
                    EndTime = null,
                    ExecutionStatusId = 1
                };

                using (var db = new YseraEntities())
                {
                    db.ExecutionRequests.Add(tmpEr);
                    output = db.SaveChanges() > 0 ? true : false;
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }

            return output;
        }

        public static List<ExecutionRequest> GetExecutionRequests()
        {
            var output = new List<ExecutionRequest>();

            try
            {
                using (var db = new YseraEntities())
                {
                    output.AddRange(db.ExecutionRequests);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }

            logger.Debug("YseraDataManager.GetExecutionRequests finished");

            return output;
        }

        public static ExecutionRequest GetExecutionRequest(int id)
        {
            ExecutionRequest output = null;

            try
            {
                using (var db = new YseraEntities())
                {
                    output = db.ExecutionRequests.Where(er => er.LoadExecId == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }

            logger.Debug($"YseraDataManager.GetExecutionRequest({id}) finished");

            return output;
        }


        public static List<ExecutionStatus> GetExecutionStatuses()
        {
            var output = new List<ExecutionStatus>();

            try
            {
                using (var db = new YseraEntities())
                {
                    output.AddRange(db.ExecutionStatuses);
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }

            logger.Debug("YseraDataManager.GetExecutionStatuses finished");

            return output;
        }

        public static ExecutionStatus GetExecutionStatus(int id)
        {
            ExecutionStatus output = null;

            try
            {
                using (var db = new YseraEntities())
                {
                    output = db.ExecutionStatuses.Where(er => er.ExecutionStatusId == id).FirstOrDefault();
                }
            }
            catch (Exception ex)
            {
                logger.Error(ex);
                throw;
            }

            logger.Debug($"YseraDataManager.GetExecutionStatus({id}) finished");

            return output;
        }
    }
}
