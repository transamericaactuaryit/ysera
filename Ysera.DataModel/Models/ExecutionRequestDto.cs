﻿using System;

namespace Ysera.DataModel.Models
{
    public class ExecutionRequestDto
    {
        public ExecutionRequestDto()
        {

        }

        public ExecutionRequestDto(ExecutionRequest er)
        {
            LoadExecId = er.LoadExecId;
            SparkAppId = er.SparkAppId;
            Description = er.Description;
            ExecutionStatusId = er.ExecutionStatusId;
            StartTime = er.StartTime;
            EndTime = er.EndTime;
        }

        public long LoadExecId { get; set; }

        public string SparkAppId { get; set; }

        public string Description { get; set; }

        public int? ExecutionStatusId { get; set; }

        public DateTime? StartTime { get; set; }

        public DateTime? EndTime { get; set; }
    }
}