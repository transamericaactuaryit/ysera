﻿namespace Ysera.DataModel.Models
{
    public class ExecutionStatusDto
    {
        public ExecutionStatusDto(ExecutionStatus es)
        {
            ExecutionStatusId = es.ExecutionStatusId;
            ExecutionStatusName = es.ExecutionStatusName;
        }

        public int ExecutionStatusId { get; set; }

        public string ExecutionStatusName { get; set; }
    }
}